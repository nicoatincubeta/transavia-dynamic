-   Remove Enabler references on **AlionAdComponent.min.js**
-   Update **abc-place** js using the values from the master build. This is the file where dynamic variables are assigned to the DOM.
-   Disable **themaTexts** in **ABC-place.js**.

-   Update **index.html** specifically **dateBrick** class and contents.
-   Disable **txtDeparture** and **txtStripe** in the HTML.
-   Center **.txtDestination** horizontally and vertically on the container by updating it in the CSS.
-   Vertically and horizontally center **.themaText** in the css via **transform: translate()**.

-   Add **$( '#a5_logo' ).attr( 'src', ABC.data['frame_5_logo_img'] )** in **setProduct()** function in **main.js**. This is going to replace the date brick with the transavia logo.
-   Remove all animation of **.seatsLeft** under **animPanels()** function. Replace it with the **.CTA** animation from master.
-   Disable **.gradient** animation on **main.js**.
-   Disable **.gradient** DOM in HTML.
-   Update **.logo** scale in **animateBanner()**. (case to case)
-   Add **.to( '.greenBackground', 0.3, { background : 'white' } )** inside **animateBanner()** on **master.js**.
-   Fix **clickLayer** using this code block found in **main.js**

```javascript
document.addEventListener("DOMContentLoaded", function () {
	AlionAd.init({
		debug_platform: "DOUBLECLICK",
		onReady: function () {
			clickLayer.addEventListener("click", function () {
				window.open(window.clickTag);
			});
			backupImg.addEventListener("click", function () {
				AlionAd.exit();
				console.log(" AlionAd.exit");
			});
			ABC.getContent(() => startBanner());
		},
	});
});
```

-   Add support for the default logo to hold multiple logo URLs.

```javascript
var logo =
	($(".logo")[0].src = ABC.data.logo) ||
	($(".logo")[0].src = ABC.data.frame_2_logo_image) ||
	($(".logo")[0].src = ABC.data.frame_3_logo_image) ||
	($(".logo")[0].src = ABC.data.frame_4_logo_image);
```

## Placement ID Status:

✅ **300x250 [301406902]**

✅ **336x280 [301135179]**

✅ **728x90 [301135863]**

✅ **320x240 [302475640]** `error message: "placement_id 302475640 not found"`

✅ **300x600 [301332772]** `error message: "placement_id 301332772 not found"`

✅ **160x600 [302154161]** `error message: "placement_id 302154161 not found"`

✅ **120x600 [302165618]** `error message: "placement_id 302165618 not found"`

✅ **320x50 [302259594]** `error message: "placement_id 302259594 not found"`
