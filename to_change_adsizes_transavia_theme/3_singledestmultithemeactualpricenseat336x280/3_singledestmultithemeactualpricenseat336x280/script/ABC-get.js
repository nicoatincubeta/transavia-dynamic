/* jshint -W117 */

/* Set Content
   ========================================================================== */
var ABC = ABC || {};

( function ( window, document ) {
   
	// The data object
	ABC.data = ABC.data || {};
   
   
   
	function getContent ( callback ) {
		getPlacementId( placementId => {
			getData( placementId, dynData => {
				callback( assignABC( dynData ) );
			} );
               
		} );
	}
   
   
	function assignABC ( dynData ) {
		ABC.data['introText']                = dynData['seamless_config']['a1_header__json']['config']['value'];
		ABC.data['introText_c']              = dynData['seamless_config']['a1_header__json']['config']['fontColour__hex'];
		ABC.data['introText_s']              = dynData['seamless_config']['a1_header__json']['config']['fontSize__number'];
        
		ABC.data['frame1_bc']                = dynData['seamless_config']['a1_backgroundColor__hex'];

		ABC.data['logo']               = dynData['seamless_config']['a1_logo__img'];
		ABC.data['logo2']              = dynData['seamless_config']['a1_logo2__img'];

		ABC.data['theme_frame_amount']       = dynData['seamless_config']['a1_themeAmount__number'];                            // still needs to be added to the code
		ABC.data['image1']                   = dynData['seamless_config']['a2_backgroundImage__img'];
		ABC.data['imageEnd']                 = dynData['seamless_config']['a2_backgroundImage__img'];
		ABC.data['themaImg1']                = dynData['seamless_config']['a2_backgroundImage__img'];
		ABC.data['frame_2_logo_image']       = dynData['seamless_config']['a2_logoImage__img'];                                 // still needs to be added to the code
		ABC.data['themaImg2']                = dynData['seamless_config']['a3_backgroundImage__img'];
		ABC.data['frame_3_logo_image']       = dynData['seamless_config']['a3_logoImage__img'];                                 // still needs to be added to the code
		ABC.data['themaImg3']                = dynData['seamless_config']['a4_backgroundImage__img'];
		ABC.data['frame_4_logo_image']       = dynData['seamless_config']['a4_logoImage__img'];                                 // still needs to be added to the code
		ABC.data['icon1']                    = dynData['seamless_config']['a5_ctaIcon__img'];
		ABC.data['Progressive_Url1']         = dynData['seamless_config']['a1_exitURL__url'];
		ABC.data['themaTxt1']                = dynData['seamless_config']['a2_themeText__json']['config']['value'];
		ABC.data['themaTxt1_c']              = dynData['seamless_config']['a2_themeText__json']['config']['fontColour__hex'];
		ABC.data['themaTxt1_s']              = dynData['seamless_config']['a2_themeText__json']['config']['fontSize__number'];
		ABC.data['themaTxt1_v']              = dynData['seamless_config']['a2_textBox__json']['config']['visible__select'];
		ABC.data['themaTxt1_bc']             = dynData['seamless_config']['a2_textBox__json']['config']['color__hex'];
		ABC.data['themaTxt1_o']              = dynData['seamless_config']['a2_textBox__json']['config']['opacity__text'];
		ABC.data['themaTxt2']                = dynData['seamless_config']['a3_themeText__json']['config']['value'];
		ABC.data['themaTxt2_c']              = dynData['seamless_config']['a3_themeText__json']['config']['fontColour__hex'];
		ABC.data['themaTxt2_s']              = dynData['seamless_config']['a3_themeText__json']['config']['fontSize__number'];
		ABC.data['themaTxt2_v']              = dynData['seamless_config']['a3_textBox__json']['config']['visible__select'];
		ABC.data['themaTxt2_bc']             = dynData['seamless_config']['a3_textBox__json']['config']['color__hex'];
		ABC.data['themaTxt2_o']              = dynData['seamless_config']['a3_textBox__json']['config']['opacity__text'];
		ABC.data['themaTxt3']                = dynData['seamless_config']['a4_themeText__json']['config']['value'];
		ABC.data['themaTxt3_c']              = dynData['seamless_config']['a4_themeText__json']['config']['fontColour__hex'];
		ABC.data['themaTxt3_s']              = dynData['seamless_config']['a4_themeText__json']['config']['fontSize__number'];
		ABC.data['themaTxt3_v']              = dynData['seamless_config']['a4_textBox__json']['config']['visible__select'];
		ABC.data['themaTxt3_bc']             = dynData['seamless_config']['a4_textBox__json']['config']['color__hex'];
		ABC.data['themaTxt3_o']              = dynData['seamless_config']['a4_textBox__json']['config']['opacity__text'];
		ABC.data['langCountryCode1']         = 'nl-NL';                                                                         //don't touch --> not needed, but creative will break without it
		ABC.data['chairs1']                  = '10';                                                                            //don't touch --> not needed, but creative will break without it
		ABC.data['currency1']   
                     = '';                                                                              //don't touch --> not needed, but creative will break without it
		// ABC.data['frame_5_headerTxt']        = dynData['seamless_config']['a5_header__text'];
		ABC.data['frame_5_headerTxt']        = dynData['seamless_config']['a5_header__json']['config']['value'];
		ABC.data['frame_5_headerTxt_s']      = dynData['seamless_config']['a5_header__json']['config']['fontSize__number'];

		ABC.data['frame_5_logo_img']         = dynData['seamless_config']['a5_logoImage__img'];
		ABC.data['frame_5_pricetext_text']   = dynData['seamless_config']['a5_priceText__json']['config']['value'];
		ABC.data['frame_5_pricetext_text_s'] = dynData['seamless_config']['a5_priceText__json']['config']['fontSize__number'];
		ABC.data['price1']                   = dynData['seamless_config']['a5_price__json']['config']['value'];
		ABC.data['price1_s']                 = dynData['seamless_config']['a5_price__json']['config']['fontSize__number'];
		ABC.data['CTA']                      = dynData['seamless_config']['a5_ctaText__json']['config']['value'];
		ABC.data['CTA_s']                    = dynData['seamless_config']['a5_ctaText__json']['config']['fontSize__number'];
		ABC.data['CTA_i']                    = dynData['seamless_config']['a5_ctaIcon__img'];
		ABC.data['customClick1']             = dynData['seamless_config']['a1_exitURL__url'];
		return ABC;
	}
   
	console.log( ABC.data );
   
	// Getting the placement ID
	function getPlacementId ( callback ) {
		setTimeout( function () {
            if (window.clickTag2.substring(0, 8) == "https://") {
            	callback(window.clickTag2.split("adurl=")[1].split("%3Fdclid")[0])
            } else {
            	callback(window.clickTag2)
            }
        }, 500);
		// 	callback( 301135179 );
		// }, 500 );
	}
   
	function getData ( placementId, callback ) {
		fetch( 'https://hv-acquisition-advertising.appspot.com/request/prospecting/theme/' + placementId )
			.then( response => response.json() )
			.then( data => {
               
				const json = JSON.parse( JSON.stringify( data ) );
				console.log( data );
				callback( json );
               
			} );
          
	}
   
	/**
        * Export the Module
        */
	ABC.getContent = ( callback ) => {
		getContent( ( data ) => {
			callback( data ); 
		} );
	};
   
        
   
       
   
   
} )( window, document );