var ABC = ABC || {};

( function ( window, document ) {
	var disclaimerTextP = $( '.disclaimer p' );
	var introTextP = $( '.introTxt p' );
	var txtCtaP = $( '.txtCTA p' );

	var endTitleText = $( '.endTitleText p' );
	function placeContent () {
		disclaimerTextP.html( ABC.data.disclaimer );
		introTextP.html( ABC.data.introText );
		txtCtaP.html( ABC.data.ctaText );

		$( '.txtThema1 p' ).html( ABC.data.themaTxt1 );
		$( '.txtThema2 p' ).html( ABC.data.themaTxt2 );
		$( '.txtThema3 p' ).html( ABC.data.themaTxt3 );
		$( '.imgOffer1' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg1 + '" )'
		);
		$( '.imgOffer2' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg2 + '" )'
		);
		$( '.imgOffer3' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg3 + '" )'
		);

		clickValUrl1 = ABC.data.customClick1;
		clickValUrl2 = ABC.data.customClick2;
		clickValUrl3 = ABC.data.customClick3;
        		//text colour
		$( '.txtThema1 p' ).css( 'color', ABC.data.themaTxt1_c );
		$( '.txtThema2 p' ).css( 'color', ABC.data.themaTxt2_c );
		$( '.txtThema3 p' ).css( 'color', ABC.data.themaTxt3_c );
		//fontsize
		$( '.txtThema1 p' ).css( 'fontSize', ABC.data.themaTxt1_s );
		$( '.txtThema2 p' ).css( 'fontSize', ABC.data.themaTxt2_s );
		$( '.txtThema3 p' ).css( 'fontSize', ABC.data.themaTxt3_s );
		if ( ABC.data.themaTxt1_v == 'true' ) {
			$( '.txtThema1' ).css( 'opacity', 0 );
		}
		if ( ABC.data.themaTxt2_v == 'true' ) {
			$( '.txtThema2' ).css( 'opacity', 0 );
		}
		if ( ABC.data.themaTxt3_v == 'true' ) {
			$( '.txtThema3' ).css( 'opacity', 0 );
		}
		/*$(".txtThema1").css("background", ABC.data.themaTxt1_bc);
        $(".txtThema2").css("background", ABC.data.themaTxt2_bc);
        $(".txtThema3").css("background", ABC.data.themaTxt3_bc);*/

		return ABC;
	}
	function formatContent () {
		var disclaimerTextP = $( '.disclaimer p' );
		var introTextP = $( '.introTxt p' );
		var introTextPc = $( '.introTxt p' ).css( 'color', ABC.data.introText_c );
		$( '.introTxt p' ).css( 'fontSize',  ABC.data.introText_s );

		var frame1 = $( '.greenBackground' ).css(
			'background-color',
			ABC.data.frame1_bc
		);
		var logo = ( $( '.logo' )[0].src = ABC.data.logo );
        if (ABC.data.logo2) {
            var logo2 = ( $( '.logo2' )[0].src = ABC.data.logo2 );
        }
        
		var endDestText = $( '.txtDestinationEnd p' );
		var endDepText = $( '.txtDepartureEnd p' );
		$( '.txtPrice p' ).css( 'fontSize', ABC.data.price1_s );

		$( '.CTA.CTA-panel.corners.shadow > img' )[0].src = ABC.data.CTA_i;
		$( '.txtCTA p' ).html( ABC.data.CTA );
		$( '.txtCTA p' ).css( 'fontSize', ABC.data.CTA_s );
		$( '.destinationBrick' ).css( 'background-color', '#2800A0' );
		$( '.destinationBrick' ).css( 'color', 'white' );

		//$(".destinationBrick > div > p > span").html(ABC.data.frame_5_headerTxt)
		//$(".destinationBrick > div > p > span").css("fontSize", ABC.data.frame_5_headerTxt_s);
		/*
	$(".txtFrom > p").css("fontSize", 12);
	if(ABC.data.themaTxt1_v == "true"){$(".priceBrick.greenPanel.corners").css("opacity", 0);}
	
	*/
		var priceEnd = $( '.txtPriceEnd p' );
		var endCtaText = $( '.txtCTA-small p' );
		var txtTopChairs = $( '.txtTop p' );
		var txtBotChairs = $( '.txtBot p' );
		/* var themaTexts = $( '.themaText p' );
		themaTexts
			.bbFitText( {
				minFontSize : 6, // minimum size to scale down to
				maxFontSize : 35, // maximum size to scale up to
				lineHeight : 1, // line-height in em

				centerHorizontal : false, // center text horizontally or not
				centerVertical : true, // center text vertically or not

				forceSingleLine : false, // force text onto single line
				scaleUpToo : true, // possibility to scale text up too

				smartBreak : true, // use smart break for long words
				smartBreakCharacter : '~', // character to break a word on
			} )
			.bbEqualizeText(); */

		disclaimerTextP.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 10, // maximum size to scale up to
			lineHeight : 1, // line-height in em

			centerHorizontal : false, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		introTextP.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 45, // maximum size to scale up to
			lineHeight : 1.05, // line-height in em

			centerHorizontal : false, // center text horizontally or not
			centerVertical : false, // center text vertically or not

			forceSingleLine : false, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		txtTopChairs.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 14, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		txtBotChairs.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 14, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		endDestText.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 32, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		endDepText.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 32, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		priceEnd.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 32, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : false, // force text onto single line
			scaleUpToo : true, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		endCtaText.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 16, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		/* ========================================================================== */

		// Return ABC object for method chaining
		return ABC;
	}

	function formatProduct () {
		//        console.log('formatting Procuct..');

		/* Use textfitter to fit dynamic text to static sized container
              ========================================================================== */

		// -------------------------- per product

		var numChairs = $( '.txtNumber p' );
		var ctaText = $( '.txtCTA p' );
		var depMaand = $( '.maand p' );
		var depDag = $( '.dag p' );
		var priceText = $( '.txtPrice p' );
		var txtDeparture = $( '.txtDeparture p' );
		var txtDestination = $( '.txtDestination p' );

		ctaText.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 40, // maximum size to scale up to
			lineHeight : 1, // line-height in em

			centerHorizontal : false, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : false, // force text onto single line
			scaleUpToo : true, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		numChairs.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 42, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		depMaand.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 20, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		depDag.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 28, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		priceText.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 60, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : false, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		txtDeparture.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 60, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : false, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		txtDestination.bbFitText( {
			minFontSize : 6, // minimum size to scale down to
			maxFontSize : 60, // maximum size to scale up to
			lineHeight : 1.2, // line-height in em

			centerHorizontal : true, // center text horizontally or not
			centerVertical : true, // center text vertically or not

			forceSingleLine : true, // force text onto single line
			scaleUpToo : true, // possibility to scale text up too

			smartBreak : false, // use smart break for long words
			smartBreakCharacter : '~', // character to break a word on
		} );

		/* ========================================================================== */

		// Return ABC object for method chaining
		return ABC;
	}

	/**
   * Export the Module
   */

	ABC.placeContent = placeContent;
	ABC.formatContent = formatContent;
	ABC.formatProduct = formatProduct;
} )( window, document );
