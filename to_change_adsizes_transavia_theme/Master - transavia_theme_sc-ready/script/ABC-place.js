var ABC = ABC || {};
( function ( window, document ) {
	var disclaimerTextP = $( '.disclaimer p' );
	var introTextP = $( '.introTxt p' );
	var txtCtaP = $( '.txtCTA p' );
	var endTitleText = $( '.endTitleText p' );
	function placeContent () {
		disclaimerTextP.html( ABC.data.disclaimer );
		introTextP.html( ABC.data.introText );
		txtCtaP.html( ABC.data.ctaText );
		$( '.txtThema1 p' ).html( ABC.data.themaTxt1 );
		$( '.txtThema2 p' ).html( ABC.data.themaTxt2 );
		$( '.txtThema3 p' ).html( ABC.data.themaTxt3 );
		$( '.imgOffer1' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg1 + '" )'
		);
		$( '.imgOffer2' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg2 + '" )'
		);
		$( '.imgOffer3' ).css(
			'background-image',
			'url( "' + ABC.data.themaImg3 + '" )'
		);
		clickValUrl1 = ABC.data.customClick1;
		clickValUrl2 = ABC.data.customClick2;
		clickValUrl3 = ABC.data.customClick3;
		//text colour
		$( '.txtThema1 p' ).css( 'color', ABC.data.themaTxt1_c );
		$( '.txtThema2 p' ).css( 'color', ABC.data.themaTxt2_c );
		$( '.txtThema3 p' ).css( 'color', ABC.data.themaTxt3_c );
		//fontsize
		$( '.txtThema1 p' ).css( 'fontSize', ABC.data.themaTxt1_s );
		$( '.txtThema2 p' ).css( 'fontSize', ABC.data.themaTxt2_s );
		$( '.txtThema3 p' ).css( 'fontSize', ABC.data.themaTxt3_s );
		if ( ABC.data.themaTxt1_v == 'true' ) {
			$( '.txtThema1' ).css( 'opacity', 0 );
		}
		if ( ABC.data.themaTxt2_v == 'true' ) {
			$( '.txtThema2' ).css( 'opacity', 0 );
		}
		if ( ABC.data.themaTxt3_v == 'true' ) {
			$( '.txtThema3' ).css( 'opacity', 0 );
		}
		/*$(".txtThema1").css("background", ABC.data.themaTxt1_bc);
        $(".txtThema2").css("background", ABC.data.themaTxt2_bc);
        $(".txtThema3").css("background", ABC.data.themaTxt3_bc);*/

		return ABC;
	}
	function formatContent () {
		var disclaimerTextP = $( '.disclaimer p' );
		var introTextP = $( '.introTxt p' );
		var introTextPc = $( '.introTxt p' ).css( 'color',  ABC.data.introText_c );
        $( '.introTxt p' ).css( 'fontSize',  ABC.data.introText_s );

		var frame1 = $( '.greenBackground' ).css(
			'background-color',
			ABC.data.frame1_bc
		);

		var logo = ( $( '.logo' )[0].src = ABC.data.logo );

        if (ABC.data.logo2) {
            var logo2 = ( $( '.logo2' )[0].src = ABC.data.logo2 );
        }

		// console.log( ABC.data.logo );
		// console.log( ABC.data.frame_2_logo_image );
		// console.log( ABC.data.frame_3_logo_image );
		// console.log( ABC.data.frame_4_logo_image );

		var endDestText = $( '.txtDestinationEnd p' );
		var endDepText = $( '.txtDepartureEnd p' );

		$( '.txtPrice p' ).css( 'fontSize', ABC.data.price1_s );
		$( '.CTA.CTA-panel.corners.shadow > img' )[0].src = ABC.data.CTA_i;
		$( '.txtCTA p' ).html( ABC.data.CTA );
		$( '.txtCTA p' ).css( 'fontSize', ABC.data.CTA_s );
		$( '.destinationBrick' ).css( 'background-color', '#2800A0' );
		$( '.destinationBrick' ).css( 'color', 'white' );

		//$(".destinationBrick > div > p > span").html(ABC.data.frame_5_headerTxt)
		//$(".destinationBrick > div > p > span").css("fontSize", ABC.data.frame_5_headerTxt_s);
		/*
	$(".txtFrom > p").css("fontSize", 12);
	if(ABC.data.themaTxt1_v == "true"){$(".priceBrick.greenPanel.corners").css("opacity", 0);}
	
	*/
		var priceEnd = $( '.txtPriceEnd p' );
		var endCtaText = $( '.txtCTA-small p' );
		var txtTopChairs = $( '.txtTop p' );
		var txtBotChairs = $( '.txtBot p' );
		/* var themaTexts = $( '.themaText p' );
		themaTexts
			.bbFitText( {
				minFontSize : 6,
				maxFontSize : 35,
				lineHeight : 1,
				centerHorizontal : false,
				centerVertical : true,
				forceSingleLine : false,
				scaleUpToo : false,
				smartBreak : false,
				smartBreakCharacter : '~',
			} )
			.bbEqualizeText(); */
		disclaimerTextP.bbFitText( {
			minFontSize : 6,
			maxFontSize : 10,
			lineHeight : 1,
			centerHorizontal : false,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		introTextP.bbFitText( {
			minFontSize : 6,
			maxFontSize : 40,
			lineHeight : 1.15,
			centerHorizontal : false,
			centerVertical : false,
			forceSingleLine : false,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		txtBotChairs.bbFitText( {
			minFontSize : 6,
			maxFontSize : 14,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		txtTopChairs.bbFitText( {
			minFontSize : 6,
			maxFontSize : 14,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		endDepText.bbFitText( {
			minFontSize : 6,
			maxFontSize : 32,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		priceEnd.bbFitText( {
			minFontSize : 6,
			maxFontSize : 32,
			lineHeight : 1,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : false,
			scaleUpToo : true,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		endCtaText.bbFitText( {
			minFontSize : 6,
			maxFontSize : 16,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		endDestText.bbFitText( {
			minFontSize : 6,
			maxFontSize : 32,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		return ABC;
	}
	function formatProduct () {
		var numChairs = $( '.txtNumber p' );
		var ctaText = $( '.txtCTA p' );
		var depMaand = $( '.maand p' );
		var depDag = $( '.dag p' );
		var priceText = $( '.txtPrice p' );
		var txtDeparture = $( '.txtDeparture p' );
		var txtDestination = $( '.txtDestination p' );
		ctaText.bbFitText( {
			minFontSize : 6,
			maxFontSize : 40,
			lineHeight : 0.9,
			centerHorizontal : false,
			centerVertical : true,
			forceSingleLine : false,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		numChairs.bbFitText( {
			minFontSize : 6,
			maxFontSize : 42,
			lineHeight : 0.9,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		depMaand.bbFitText( {
			minFontSize : 6,
			maxFontSize : 20,
			lineHeight : 1,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		depDag.bbFitText( {
			minFontSize : 6,
			maxFontSize : 28,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		priceText.bbFitText( {
			minFontSize : 6,
			maxFontSize : 60,
			lineHeight : 1,
			centerHorizontal : true,
			centerVertical : false,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		txtDeparture.bbFitText( {
			minFontSize : 6,
			maxFontSize : 60,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		txtDestination.bbFitText( {
			minFontSize : 6,
			maxFontSize : 60,
			lineHeight : 1.2,
			centerHorizontal : true,
			centerVertical : true,
			forceSingleLine : true,
			scaleUpToo : false,
			smartBreak : false,
			smartBreakCharacter : '~',
		} );
		return ABC;
	}
	ABC.placeContent = placeContent;
	ABC.formatContent = formatContent;
	ABC.formatProduct = formatProduct;
} )( window, document );
