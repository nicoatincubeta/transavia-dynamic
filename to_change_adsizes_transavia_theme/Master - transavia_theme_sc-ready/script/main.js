var clickLayer = document.getElementsByClassName( 'clickLayer' )[0];
var bannerWidth = 300;
var bannerHeight = 250;
var main_tl = new TimelineMax();
var second_tl = new TimelineMax( { delay : 1, repeat : 1, repeatDelay : 1.5 } );
var panel_tl = new TimelineMax();
var exitPanel_tl = new TimelineMax();
var theExit = 1;
var productToShow = 1;
var showAnimation = true;
var backupImg = document.getElementsByClassName( 'backupImg' )[0];

function showError () {
	showAnimation = false;
	$( '.backupImg' ).css( 'display', 'block' );
	fadePreloader();
}

function setProduct () {
	var monthABC = 'Dit is een test';
	$( '.txtPrice p' ).html(
		'<sup>' +
      ABC.data['currency' + productToShow] +
      '</sup>' +
      ABC.data['price' + productToShow]
	);

	$( '.txtDestination p' ).html( ABC.data.frame_5_headerTxt );
	$( '.txtDestination p' ).css( 'fontSize', ABC.data.frame_5_headerTxt_s );

	$( '.txtFrom p' ).html( ABC.data['frame_5_pricetext_text'] );
	$( '.txtFrom p' ).css( 'fontSize', ABC.data['frame_5_pricetext_text_s'] );

	$( '#a5_logo' ).attr( 'src', ABC.data['frame_5_logo_img'] );
	$( '.txtNumber p' ).html( ABC.data['chairs' + productToShow] );
	$( '.txtTop p' ).html( 'Voor deze prijs' );
	if ( ABC.data['chairs' + productToShow] <= 1 ) {
		$( '.txtBot p' ).html( 'stoel over' );
	} else {
		$( '.txtBot p' ).html( 'stoelen over' );
	}
	theExit = 1;
	ABC.formatProduct();
}

function animPanels () {
	panel_tl
		.set( '.destinationBrick', { x : -bannerWidth, autoAlpha : 1 } )
		.set( '.dateBrick', { y : 0, x : 0, scale : 0, autoAlpha : 1 } )
		.set( '.priceBrick', { y : bannerHeight, autoAlpha : 1 } )
		.set( '.CTA', { scaleY : 0, x : 0, y : 0, autoAlpha : 0 } )
		.set( '.seatsLeft', { scaleY : 1, x : 0, y : bannerWidth, autoAlpha : 0 } )
		.to( '.destinationBrick', 0.5, { x : 0, ease : Expo.easeOut } )
		.to( '.priceBrick', 0.5, { y : 0, ease : Expo.easeOut }, '-=0.5' )
		.to( '.dateBrick', 0.5, { scale : 1, ease : Back.easeOut }, '-=0.5' )

		.to( '.CTA', 0.5, { y : 0, scaleY : 1, autoAlpha : 1, ease : Expo.easeOut } )
		.to( '.CTA', 0.5, {
			y : 0,
			scaleY : 0,
			autoAlpha : 0,
			ease : Expo.easeIn,
			delay : 1.5,
		} )
		.to( '.CTA', 0.5, { scaleY : 1, autoAlpha : 1, ease : Power4.easeOut } )
		.to( '.CTA', 0.5, {
			scaleY : 0,
			autoAlpha : 1,
			ease : Power4.easeIn,
			delay : 1.5,
		} )
		.to( '.CTA', 0.5, { y : 0, scaleY : 1, autoAlpha : 1, ease : Expo.easeOut } );
}

function exitPanels () {
	exitPanel_tl
		.to( '.CTA', 0.4, { x : bannerWidth, ease : Circ.easeIn }, '+=1' )
		.to(
			'.destinationBrick',
			0.4,
			{ x : -bannerWidth, ease : Circ.easeIn },
			'-=0.4'
		)
		.to( '.dateBrick', 0.4, { y : -bannerHeight, ease : Circ.easeIn }, '-=0.4' )
		.to( '.priceBrick', 0.4, { y : bannerHeight, ease : Circ.easeIn }, '-=0.4' )
		.call( function () {
			setProduct();
		} );
}

function animateBanner () {
	main_tl
		.call( function () {
			setProduct();
		} )
		.set( '.gradient', { autoAlpha : 0 } )
		.set( '.endTitle', { autoAlpha : 0 } )
		.set( '.themaBrick', { autoAlpha : 0, scaleY : 0, x : 0, y : 0, autoAlpha : 0 } )
		.set( '.txtThema1', { autoAlpha : 0 } )
		.set( '.txtThema2', { autoAlpha : 0 } )
		.set( '.txtThema3', { autoAlpha : 0 } )
		.set( '.destinationBrick', { autoAlpha : 0 } )
		.set( '.dateBrick', { autoAlpha : 0 } )
		.set( '.priceBrick', { autoAlpha : 0 } )
		.set( '.CTA', { autoAlpha : 0 } )
		.set( '.seatsLeft', { autoAlpha : 0 } )
		.set( '.logo', { y : 12, scale : 1.8, transformOrigin : 'bottom right' } )
        .set( '.logo2', { autoAlpha: 0, scale: 0.6, transformOrigin: 'bottom left' } )

		.from( '.introTxt', 1.5, { y : -20, ease : Expo.easeOut, autoAlpha : 0 } )
		.from( '.logo', 1.5, { y : 20, ease : Expo.easeOut, autoAlpha : 0 }, '-=1.5' )

		.to( '.introTxt', 0.7, { autoAlpha : 0, delay : 1 } )

		.to( '.gradient', 0.5, { autoAlpha : 1 } )
		.to( '.logo', 0.8, { y : 0, scale : 0.7, ease : Power3.easeInOut }, '-=1' )
        .to( '.logo2', 0.8, { autoAlpha: 1, ease : Power3.easeInOut }, '-=1' )

		.from( '.imgOffer1', 0.7, { autoAlpha : 0 }, '-=0.7' )

		.set( '.txtThema1', { autoAlpha : 1 } )
		.to( '.themaBrick', 0.5, {
			y : 0,
			scaleY : 1,
			// autoAlpha : ABC.data.themaTxt1_o,
			autoAlpha : 1,
			background : ABC.data.themaTxt1_bc + ( ABC.data.themaTxt1_o * 100 ),
			ease : Expo.easeOut,
		} )
		.to( '.themaBrick', 0.5, {
			scaleY : 0,
			autoAlpha : 0,
			ease : Power4.easeIn,
			delay : 2,
		} )
		.to( '.imgOffer1', 0.7, { autoAlpha : 0 } )
		.from( '.imgOffer2', 0.5, { autoAlpha : 0 }, '-=0.6' )

		.set( '.txtThema1', { autoAlpha : 0 } )
		.set( '.txtThema2', { autoAlpha : 1 } )
		.to( '.themaBrick', 0.5, {
			y : 0,
			scaleY : 1,
			// autoAlpha : ABC.data.themaTxt2_o,
			autoAlpha : 1,
			background : ABC.data.themaTxt2_bc + ( ABC.data.themaTxt2_o * 100 ),
			ease : Expo.easeOut,
		} )
		.to( '.themaBrick', 0.5, {
			scaleY : 0,
			autoAlpha : 0,
			ease : Power4.easeIn,
			delay : 2,
		} )
		.to( '.imgOffer2', 0.7, { autoAlpha : 0 } )
		.from( '.imgOffer3', 0.5, { autoAlpha : 0 }, '-=0.6' )

		.set( '.txtThema2', { autoAlpha : 0 } )
		.set( '.txtThema3', { autoAlpha : 1 } )
		.to( '.themaBrick', 0.5, {
			y : 0,
			scaleY : 1,
			// autoAlpha : ABC.data.themaTxt3_o,
			autoAlpha : 1,
			background : ABC.data.themaTxt3_bc + ( ABC.data.themaTxt3_o * 100 ),
			ease : Expo.easeOut,
		} )
		.to( '.themaBrick', 0.5, {
			scaleY : 0,
			autoAlpha : 0,
			ease : Power4.easeIn,
			delay : 2,
		} )
		.to( '.greenBackground', 0.3, { background : 'white' } )
		.to( '.gradient', 0, { autoAlpha : 0 } )
		.to( '.imgOffer3', 0.7, { autoAlpha : 0 } )

		.to( ['.logo', '.logo2'], 0.2, { autoAlpha : 0 }, '-=1' )

		.to( '.imgOffer1', 0.5, { autoAlpha : 0 }, '-=0.6' )
		.set( '.animPanels', { onComplete : animPanels }, '-=0.6' )
	    //.from(".gradient", 0.6, { autoAlpha: 0 }, "-=0.6")
		.from( '.disclaimer', 0.6, { autoAlpha : 0 }, '-=0.6' )
		.from( '.delay', 1, { autoAlpha : 0, delay : 2 } );
}

document.addEventListener( 'DOMContentLoaded', function () {
	AlionAd.init( {
		debug_platform : 'DOUBLECLICK',
		onReady : function () {
			clickLayer.addEventListener( 'click', function () {window.open( window.clickTag );} );
			backupImg.addEventListener( 'click', function () {
				AlionAd.exit();
				console.log( ' AlionAd.exit' );
			} );
			ABC.getContent( () => startBanner() );
		}
	} );
} );

function startBanner () {
	ABC.placeContent().formatContent().formatProduct();
	animateBanner();
	fadePreloader();
}

function fadePreloader () {
	var preloader = document.getElementById( 'preloader' );
	var fadeTime = 200;
	preloader.style.transition = 'all ' + fadeTime + 'ms ease-out 0s';
	preloader.style.opacity = '0';
	setTimeout( function () {
		preloader.style.display = 'none';
	}, fadeTime );
}
